=========================
Introduction
=========================


Welcome to these **Numerical Tours of Computational Mechanics with FEniCS**.


------------------------
What is it about ?
------------------------


These numerical tours will introduce you to a wide variety of topics in
computational continuum and structural mechanics using the finite element software FEniCS.
Many covered topics can be considered as standard and will help the reader in
getting started with FEniCS using solid mechanics examples.

Other topics will also be more exploratory and will reflect currently investigated research topics,
illustrating the versatility of FEniCS.

The full set of demos can be obtained from the *COmputational MEchanics Toolbox* (COMET) available at
https://gitlab.enpc.fr/jeremy.bleyer/comet-fenics.

--------------------
Citing and license
--------------------

If you find these demos useful for your research work, please consider citing them using the following
Zenodo DOI https://doi.org/10.5281/zenodo.1287832

.. code-block:: none

	@article{bleyer2018numericaltours,
	title={Numerical Tours of Computational Mechanics with FEniCS},
	DOI={10.5281/zenodo.1287832},
	publisher={Zenodo},
	author={Jeremy Bleyer},
	year={2018}}

All this work is licensed under the `Creative Commons Attribution-ShareAlike 4.0 International License <http://creativecommons.org/licenses/by-sa/4.0/>`_ |license|.

.. |license| image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png

-----------------------
How do I get started ?
-----------------------

You can find instructions on how to install FEniCS on the FEniCS project website
http://fenicsproject.org. In the following numerical tours, we will use the
Python interface for the different FEniCS scripts. These demos are compatible with
FEniCS 2018.1.0 but many should work with older versions after minor changes.

FEniCS is also distributed along with an important number of documented or
undocumented examples, some of them will be revisited in these tours but do not
hesitate over looking at other interesting examples.

In the following, we will assume that readers possess basic knowledge of FEniCS commands.
In particular, we advise you to go first through the documentation and tutorials https://fenicsproject.org/tutorial/
if this is not the case.

----------------------
About the author
----------------------

`Jeremy Bleyer <https://sites.google.com/site/bleyerjeremy/>`_ is a researcher
in Solid and Structural Mechanics at `Laboratoire Navier <http://navier.enpc.fr>`_,
a joint research unit of `Ecole Nationale des Ponts et Chaussées <http://www.enpc.fr>`_,
`IFSTTAR <http://www.ifsttar.fr>`_ and `CNRS <http://www.cnrs.fr>`_ (UMR 8205).

email: jeremy.bleyer@enpc.fr

.. image:: _static/logo_Navier.png
   :scale: 8 %
   :align: left
.. image:: _static/logo_tutelles.png
   :scale: 20 %
   :align: right






