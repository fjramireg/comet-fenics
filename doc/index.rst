.. Numerical tours of continuum mechanics using FEniCS documentation master file, created by
   sphinx-quickstart on Wed Jun  8 21:25:10 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Numerical tours of Computational Mechanics using FEniCS
==================================================================

Contents:

.. toctree::
   :maxdepth: 2

   intro
   linear_problems
   homogenization
   nonlinear_problems
   beams_and_plates
   tips_and_tricks



