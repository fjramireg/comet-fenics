.. Numerical tours of continuum mechanics using FEniCS documentation master file, created by
   sphinx-quickstart on Wed Jun  8 21:25:10 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Beams and plates
=================

Contents:

.. toctree::
   :maxdepth: 1

   demo/timoshenko/beam_buckling.ipynb
   demo/reissner_mindlin/reissner_mindlin_quads.py.rst
   demo/reissner_mindlin/reissner_mindlin_dg.py.rst



